<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cmbizness.ru/css/app.css?id=592411089da7656b65fc" rel="stylesheet">
</head>
<body>
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-12 osagoform">

            <h4>Рассчитать стоимость ОСАГО</h4>
            <div class="rounded bg-light border p-3">
                @include('form.form')
            </div>

        </div>
    </div>
</div>
@stack('scripts')
</body>
</html>
