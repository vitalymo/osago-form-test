<form method="POST" action="#" accept-charset="UTF-8"
      enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="row align-items-end">
        @include('form.inputs.period.index')
        @include('form.inputs.prev_polis.index')
    </div>
    <hr>

    @include('form.inputs.about.index')
    {{--    @include('form.inputs.owner.index')--}}
    @include('form.inputs.contacts.index')

    @include('form.buttons.index')
</form>


@push('scripts')
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            // csrf protection
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // load on click listener
            $('#loadBtn').click(function (e) {
                e.preventDefault();
                load()
            });
        });

        function load() {
            $.ajax({
                url: "{{ route('handle') }}",
                method: 'post',
                success: function (result) {
                    renderWithContent(result)
                }
            });
        }

        function renderWithContent(content) {
            // установка данных в качестве значений полей
            $("input[name='osago_mark']").val(content.tech_data.brand.name.normalized)
            $("input[name='osago_model']").val(content.tech_data.model.name.normalized)
            $("input[name='osago_year_auto']").val(content.tech_data.year)
            $("input[name='osago_power_auto']").val(content.tech_data.engine.power.hp)
            $("select[name='cost'] option")
                .removeAttr('selected')
                .filter(function () {
                    return $(this).html() === content.tech_data.engine.fuel.type;
                })
                .attr('selected', true)
        }
    </script>
@endpush
