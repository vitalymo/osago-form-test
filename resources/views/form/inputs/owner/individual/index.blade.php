<div class="sobstv" style="display: none;">
    <div class="row">
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-4 form-group">
                    <input
                        onkeyup="if(/[^a-zA-Zа-яА-ЯёЁ .]/i.test(this.value)){this.value=this.value.substring(0, this.value.length - 1); }"
                        id="owner_surname" class="form-control " autocomplete="off"
                        placeholder="Фамилия" name="owner_surname" type="text">
                </div>
                <div class="col-lg-4 form-group">
                    <input
                        onkeyup="if(/[^a-zA-Zа-яА-ЯёЁ .]/i.test(this.value)){this.value=this.value.substring(0, this.value.length - 1); }"
                        id="owner_name" class="form-control " autocomplete="off"
                        placeholder="Имя" name="owner_name" type="text">
                </div>
                <div class="col-lg-4 form-group">
                    <input
                        onkeyup="if(/[^a-zA-Zа-яА-ЯёЁ .]/i.test(this.value)){this.value=this.value.substring(0, this.value.length - 1); }"
                        id="owner_middleName" class="form-control " autocomplete="off"
                        placeholder="Отчество" name="owner_middleName" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-lg form-group">
                    <input id="owner_birthday" class="form-control realDate" autocomplete="off"
                           placeholder="Дата рождения" name="owner_birthday" type="text">
                </div>
                <div class="col-lg form-group">
                    <input id="owner_passport" class="form-control " autocomplete="off"
                           maxlength="11" data-mask="0000 000000"
                           placeholder="Серия и номер паспорта" name="owner_passport"
                           type="text">
                </div>
                <div class="col-lg form-group">
                    <input id="owner_date_pass" class="form-control realDate" autocomplete="off"
                           placeholder="Дата выдачи паспорта" name="owner_date_pass"
                           type="text">
                </div>
                <div class="col-lg form-group">
                    <input id="owner_issue_pass_code" class="form-control " autocomplete="off"
                           data-mask="000-000" placeholder="Код подразделения"
                           name="owner_issue_pass_code" type="text" maxlength="7">
                </div>
            </div>
            <div class="row">
                <div class="col-lg form-group">
                    <input id="owner_issue_pass" class="form-control " autocomplete="off"
                           placeholder="Кем выдан паспорт" name="owner_issue_pass" type="text">
                </div>

                <div class="col-lg form-group">
                    <input id="owner_registration_pass" class="form-control " autocomplete="off"
                           placeholder="Адрес регистрации собственника полностью"
                           name="owner_registration_pass" type="text">
                </div>
            </div>
            <div class="form-check ">
                <input class="form-check-input" id="vDriver" checked="checked" name="vDriver"
                       type="checkbox" value="0">
                <label for="vDriver" class="form-check-label">Собственник является
                    водителем</label>
            </div>
        </div>
        <div class="col-lg-3">
            <strong>Прикрепить документы</strong>
            <div class="d-flex align-items-center form-group">
                <div class="col">
                    <div class="row">
                        <input class="form-control-file" name="files[]" type="file">
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <select class="custom-select" name="selectOwners[]">
                            <option value="" selected="selected" disabled="disabled">Тип файла
                            </option>
                            <option value="0">Паспорт РФ</option>
                            <option value="1">Военный билет</option>
                            <option value="2">Иностранный паспорт</option>
                            <option value="3">Вид на жительство</option>
                            <option value="4">Паспорт моряка</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="addButton btn btn-success mb-3">Добавить ещё файл</div>

        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
        </div>
    </div>
    <div class="row justify-content-center driver_add">
        <div class="col-lg-4">
            <div class="btn btn-info text-light btn-block" id="free_drivers">Без ограничения
                водителей
            </div>
        </div>
    </div>

    <div class="driver" data-driver="1">
        <strong>Водитель 1</strong>
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg form-group">
                        <input id="drivers10last_name" class="form-control " autocomplete="off"
                               placeholder="Фамилия" name="drivers10last_name" type="text">
                    </div>
                    <div class="col-lg-4 form-group">
                        <input id="drivers10first_name" class="form-control " autocomplete="off"
                               placeholder="Имя" name="drivers10first_name" type="text">
                    </div>
                    <div class="col-lg-4 form-group">
                        <input id="drivers10middle_name" class="form-control "
                               autocomplete="off" placeholder="Отчество"
                               name="drivers10middle_name" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <input id="drivers10birth_date" class="form-control realDate"
                               autocomplete="off" placeholder="Дата рождения"
                               name="drivers10birth_date" type="text">
                    </div>
                    <div class="col-lg-4 form-group">
                        <input id="drivers10current_driver_license"
                               class="form-control serNumbVU" autocomplete="off" maxlength="11"
                               placeholder="Серия и номер в/у"
                               name="drivers10current_driver_license" type="text">
                    </div>
                    <div class="col-lg-4 form-group">
                        <input id="drivers10date_dl" class="form-control realDate"
                               autocomplete="off" maxlength="11"
                               placeholder="Дата выдачи действуещего в/у"
                               name="drivers10date_dl" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <input id="drivers10date_first_dl" class="form-control realDate"
                               autocomplete="off" maxlength="11"
                               placeholder="Дата выдачи первого в/у"
                               name="drivers10date_first_dl" type="text">
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <strong>Прикрепить документы</strong>
                <div class="d-flex align-items-center form-group">
                    <div class="col">
                        <div class="row">
                            <input class="form-control-file" name="files[]" type="file">
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <select class="custom-select" name="selectDrivers[]">
                                <option value="" selected="selected" disabled="disabled">Тип
                                    файла
                                </option>
                                <option value="0">Водительское удостоверение</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="addButton btn btn-success mb-3">Добавить ещё файл</div>

            </div>
        </div>
    </div>

    <div class="row justify-content-center driver_add">
        <div class="col-lg-4">
            <div class="btn btn-success driver-add btn-block">Добавить водителя</div>
        </div>
    </div>
</div>
