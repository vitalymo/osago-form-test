<div class="sobstv_ul" style="display: none;">
    <div class="row">
        <div class="col-lg-9">
            <div class="row ">
                <div class="col-lg-2">
                    <div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" name="ul_resident" autocomplete="off"
                                   checked=""> Резидент
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="ul_resident" autocomplete="off"> Не
                            резидент
                        </label>
                    </div>
                </div>
                <div class="col-lg-2 form-group">
                    <select class="custom-select" id="ul_form" name="ul_form">
                        <option selected="selected" value="" disabled="disabled">Форма</option>
                        <option value="0">ОАО</option>
                        <option value="1">ООО</option>
                        <option value="2">Иное, укажу в наименовании</option>
                    </select>
                </div>
                <div class="col-lg-8 form-group">
                    <input id="ul_name" class="form-control " autocomplete="off"
                           placeholder="Наименование" name="ul_name" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 form-group">
                    <input id="ul_inn" class="form-control " autocomplete="off" data-mask="0#"
                           placeholder="ИНН" name="ul_inn" type="text">
                </div>
                <div class=" col-lg-3 form-group">
                    <input id="ul_kpp" class="form-control " autocomplete="off" data-mask="0#"
                           placeholder="КПП" name="ul_kpp" type="text">
                </div>
                <div class="col-lg-3 form-group">
                    <input id="ul_ser_nom" class="form-control " autocomplete="off"
                           placeholder="Серия/Номер Свитедельства о рег." name="ul_ser_nom"
                           type="text">
                </div>
                <div class="col-lg-3 form-group">
                    <input id="ul_sv_date" class="realDate form-control " autocomplete="off"
                           placeholder="Дата выдачи" name="ul_sv_date" type="text">
                </div>
            </div>
            <div class=" row">
                <div class="col-lg-6 form-group">
                    <input id="insurant_phone" class="form-control " autocomplete="off"
                           placeholder="Телефон" name="ul_phone" type="text">
                </div>
                <div class="col-lg-6 form-group">
                    <input id="ul_email" class=" form-control " autocomplete="off"
                           placeholder="email" name="ul_email" type="email">
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <input id="ul_address" class=" form-control " autocomplete="off"
                           placeholder="Адрес" name="ul_address" type="text">
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="d-flex align-items-center form-group">
                <div class="col">
                    <div class="row">
                        <input class="form-control-file" name="files[]" type="file">
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <select class="custom-select" name="selectOwners[]">
                            <option value="" selected="selected" disabled="disabled">Тип файла
                            </option>
                            <option value="0">Свидетельство о регистрации</option>
                            <option value="1">ИНН, ОГРН</option>
                            <option value="2">Карточка организации</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="addButton btn btn-success mb-3">Добавить ещё файл</div>

        </div>
    </div>
    <div class="alert alert-info">
        Для юридических лиц доступна только страховка без ограничения водителей
    </div>
</div>
