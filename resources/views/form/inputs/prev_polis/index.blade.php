<div class="col-lg-5">
    <h4>Предыдущий полис</h4>
    <div class="row">
        <div class="col-lg-6">
            @include('form.inputs.prev_polis.series')
        </div>
        <div class="col-lg-6">
            @include('form.inputs.prev_polis.number')
        </div>
    </div>
</div>
