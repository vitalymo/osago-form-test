<h4>Информация об автомобиле</h4>

<div class="row">
    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-4">
                @include('form.inputs.about.auto_number')

                <div class="row">
                    @include('form.inputs.about.brand')
                    @include('form.inputs.about.model')
                </div>
                @include('form.inputs.about.type')
                <div class="form-group">
                    <div class="form-check ">
                        <input class="form-check-input" id="osago_trailer" name="osago_trailer"
                               type="checkbox" value="0">
                        <label for="osago_trailer" class="form-check-label">Авто эксплуатируется
                            с прицепом (для кат.С)</label>
                    </div>
                </div>
                <div class="form-group">
                    <input id="osago_year_auto" class="form-control " autocomplete="off"
                           maxlength="4" placeholder="Год выпуска" data-mask="0#"
                           name="osago_year_auto" type="text">
                </div>
                <div class="row form-group">
                    <div class="col-6">
                        <input id="osago_diagnost_auto" class="form-control " autocomplete="off"
                               placeholder="Диагностическая карта" name="osago_diagnost_auto"
                               type="text">
                    </div>
                    <div class="col-6">
                        <input id="osago_diagnost_date_auto" class="form-control date"
                               autocomplete="off" placeholder="Действует до"
                               name="osago_diagnost_date_auto" type="text">
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">

                    <input id="osago_power_auto" class="form-control " autocomplete="off"
                           placeholder="Мощность двигателя(л.с.)" name="osago_power_auto"
                           type="text">
                </div>
                <div class="form-group">
                    <select id="cost" class="custom-select" name="cost">
                        <option selected="selected" value="" disabled="disabled">Тип двигателя
                        </option>
                        <option value="benz">Бензиновый</option>
                        <option value="disel">Дизельный</option>
                        <option value="hybrid">Гибрид</option>
                        <option value="electro">Электродвигатель</option>
                    </select>
                </div>
                <div class="form-group">
                    <select id="goal" class="custom-select" name="goal">
                        <option selected="selected" value="" disabled="disabled">Цель
                            использования авто
                        </option>
                        <option value="0">Личные</option>
                        <option value="1">Учебная езда</option>
                        <option value="2">Инкассация</option>
                        <option value="3">Скорая помощь</option>
                        <option value="4">Такси</option>
                        <option value="5">Дорожные и специальные ТС</option>
                        <option value="6">Регулярные/по заказам пассажирские перевозки</option>
                        <option value="7">Перевозка опасных и легко воспламеняющихся грузов
                        </option>
                        <option value="8">Экстренные и коммунальные службы</option>
                        <option value="9">Прокат/Краткосрочная аренда</option>
                        <option value="10">Прочее</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" class="options_vin" name="options_vin" id="vin1"
                                   autocomplete="off" value="vin" checked=""> VIN
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" class="options_vin" name="options_vin" id="vin2"
                                   autocomplete="off" value="kuzov"> Кузов
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" class="options_vin" name="options_vin" id="vin3"
                                   autocomplete="off" value="shassi"> Шасси
                        </label>
                    </div>
                    <input id="osago_vin_auto" class="form-control " autocomplete="off"
                           placeholder="Номер VIN" name="osago_vin_auto" type="text">
                </div>
                <div class="form-group">

                    <div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" name="sts_options" id="sts" value="sts"
                                   autocomplete="off" checked=""> СТС
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="sts_options" id="pts" value="pts"
                                   autocomplete="off"> ПТС
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="sts_options" id="epts" value="epts"
                                   autocomplete="off"> ЭПТС
                        </label>
                    </div>
                    <input id="osago_sts_auto" class="form-control" autocomplete="off"
                           placeholder="Серия, номер СТС" name="osago_sts_auto" type="text">
                </div>
                <div class="form-group">
                    <input id="osago_sts_date" class="form-control realDate" autocomplete="off"
                           placeholder="Дата выдачи" name="osago_sts_date" type="text">
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
    <div class="col-lg-3">
        <div class="file_block">
            <div class="d-flex align-items-center form-group">
                <div class="col">
                    <div class="row">
                        <input class="form-control-file" name="files[]" type="file">
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <select class="custom-select" name="selectCars[]">
                            <option value="" selected="selected" disabled="disabled">Тип файла
                            </option>
                            <option value="0">Паспорт ТС</option>
                            <option value="1">Свидетельство о регистрации ТС</option>
                            <option value="2">Паспорт самоходной машины</option>
                            <option value="3">Техпаспорт</option>
                            <option value="4">Технический талон</option>
                            <option value="5">Техпаспорт иностранного государства</option>
                            <option value="6">Паспорт ТС иностранного государства</option>
                            <option value="7">Свидетельство о регистрации ТС иностранного
                                государства
                            </option>
                            <option value="8">Технический талон иностранного государства
                            </option>
                            <option value="9">Паспорт самоходной машины иностранного
                                государства
                            </option>
                            <option value="10">Аналогичные документы на ТС</option>
                            <option value="11">Электронный паспорт ТС</option>
                            <option value="12">ГТО (талон государственного технического
                                осмотра)
                            </option>
                            <option value="13">ТО (талон техосмотра)</option>
                            <option value="14">МСТО (международный сертификат технического
                                осмотра)
                            </option>
                            <option value="15">Диагностическая карта</option>
                            <option value="16">СТС</option>
                            <option value="17">ПТС</option>
                            <option value="18">ЭПТС</option>
                            <option value="19">Полис ОСАГО</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="addButton btn btn-success mb-3">Добавить ещё файл</div>

        </div>
    </div>
</div>
<hr>
