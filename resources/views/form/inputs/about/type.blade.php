<div class="form-group">
    <select class="custom-select" id="autoModel" style="display: none;"
            name="autoModel">
        <option selected="selected" value="" disabled="disabled">Укажите вид
            транспортного средства
        </option>
        <option value="0">Другое ТС Легковое</option>
        <option value="1">Другое ТС Легковое(ВЕ)</option>
        <option value="2">Другое ТС Грузовой</option>
        <option value="3">Другое ТС Грузовой(СЕ)</option>
        <option value="4">Другое ТС Автобусы</option>
        <option value="5">Другое ТС Автобусы(DE)</option>
        <option value="6">Другое ТС Мотоциклы</option>
        <option value="7">Другое ТС Мотоциклы(М)</option>
        <option value="8">Другое ТС Трактора и Иные</option>
        <option value="9">Другое ТС Трамвай(Tm)</option>
        <option value="10">Другое ТС Троллейбусы(Tb)</option>
    </select>
</div>
