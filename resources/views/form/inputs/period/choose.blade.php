<label for="">Выберите период</label>
<select class="custom-select" id="osago_period" name="osago_period">
    <option value="" disabled="disabled">Период использования ТС</option>
    <option value="12" selected="selected">12 месяцев</option>
    <option value="6">6 месяцев</option>
    <option value="3">3 месяцев</option>
</select>
