<div class="col-lg-7">
    <h4>Период страхования</h4>
    <div class="row">
        <div class="col-lg-4">
            @include('form.inputs.period.choose')
        </div>
        <div class="col-lg-4">
            @include('form.inputs.period.start')
        </div>
        <div class="col-lg-4">
            @include('form.inputs.period.end')
        </div>
    </div>
</div>
