<h4>Контакты</h4>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <input id="insurant_phone" class="form-control" autocomplete="off"
                   placeholder="Телефон" name="insurant_phone" type="text">
            <div class="text-danger small">Согласно указанию ЦБ РФ №4723-У, для оформления
                е-ОСАГО используется номер телефона клиента.
            </div>
        </div>
        <div class="form-group">
            <input id="insurant_email" class="form-control " autocomplete="off"
                   placeholder="Электронная почта" name="insurant_email" type="email">
        </div>
    </div>
    <div class="col-lg-6 d-flex">
        <div class="form-group d-flex align-self-stretch w-100">
                                    <textarea class="form-control align-self-stretch w-100" rows="3"
                                              placeholder="Комментарий" name="comment" cols="50"></textarea>
        </div>
    </div>
</div>
<hr>
