<?php

namespace App\Providers;

use App\Services\AvtocodAPI;
use App\Services\AvtocodFile;
use App\Services\IAvtocod;
use Illuminate\Support\ServiceProvider;

class AvtocodServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IAvtocod::class, function ($app) {
//            return new AvtocodAPI();
            return new AvtocodFile();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
