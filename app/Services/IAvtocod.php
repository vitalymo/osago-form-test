<?php


namespace App\Services;


interface IAvtocod
{
    /**
     * загрузка данных отчета по vin номеру. пример: Z94CB41AAGR323020
     * @param $vin
     * @return array
     */
    public function load($vin);
}
