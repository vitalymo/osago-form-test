<?php


namespace App\Services;


class AvtocodFile implements IAvtocod
{
    private $path;

    public function __construct()
    {
        $this->path = storage_path('/test.json');
    }

    /**
     * см. интерфейс
     * @param $vin
     * @return array
     */
    public function load($vin)
    {
        return json_decode(file_get_contents($this->path), true);
    }
}
