<?php

namespace App\Services;

use Avtocod\B2BApi\Client;
use Avtocod\B2BApi\Settings;

class AvtocodAPI implements IAvtocod
{
    /**
     * текущий пользователь
     * @var Client|null
     */
    protected $client = null;

    public function __construct()
    {
        $token = env('TOKEN');
        $settings = new Settings($token);
        $this->client = new Client($settings);
    }

    /**
     * см. интерфейс
     * @param $vin
     * @return array
     */
    public function load($vin)
    {
        $reportTypeUID = $this->getFirstReportTypeUID();

        $reportUID = $this->makeReport($reportTypeUID, $vin);

        $content = $this->client->userReport($reportUID)->first()->getContent();

        return $content->getContent();
    }

    /**
     * получение uid самого первого типа отчета, который можно сгенерировать
     * @return string
     */
    private function getFirstReportTypeUID()
    {
        $reportTypeUID = $this->client->userReportTypes()->getData()[0]->getUid();
        return $reportTypeUID;
    }

    /**
     * генерация нового отчета
     * @param $reportTypeUID
     * @return string
     */
    private function makeReport($reportTypeUID, $value)
    {
        $reportUID = $this->client
            ->userReportMake($reportTypeUID, 'VIN', $value, null, true)
            ->first()
            ->getReportUid();

        // ожидать конца создания отчета
        while (true) {
            if ($this->client->userReport($reportUID, false)->first()->isCompleted()) {
                break;
            }

            \sleep(1);
        }

        return $reportUID;
    }
}
