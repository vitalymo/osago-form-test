<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Services\IAvtocod;

class FormController extends Controller
{
    /**
     * обработчик загрузки данных
     * @param IAvtocod $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(IAvtocod $client)
    {
        $vim = 'Z94CB41AAGR323020';
        $data = $this->load($client, $vim);

        return response()->json($data);
    }

    /**
     * загрузчик данных по vim.
     * вынесен в отдельный метод, т.к. используется тестовым маршрутом
     * @param IAvtocod $client
     * @param $vim
     * @return mixed
     */
    public function load(IAvtocod $client, $vim)
    {
        return $client->load($vim);
    }
}
