<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Form\FormController;

// отображение формы
Route::view('/', 'index');
Route::post('/load', [FormController::class, 'handle'])->name('handle');

// dev
Route::get('/test', [FormController::class, 'load'])->name('load');
